package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/go-redis/redis"
)

func main() {
	http.HandleFunc("/", helloworld)
	log.Fatal(http.ListenAndServe(":3214", nil))
}

func helloworld(w http.ResponseWriter, r *http.Request) {
	client := redis.NewClient(&redis.Options{
		Addr:     "redis:6379",
		Password: "",
		DB:       0,
	})

	var vs string
	visits, err := client.Incr("couter").Result()
	if err != nil {
		log.Printf("Connect  redis errors: %s", err.Error())
		vs = "Visits: cannot connect to Redis, counter disabled"
	} else {
		fmt.Println(visits)
		vs = fmt.Sprintf("Visits:%d", visits)
	}

	name, err := os.Hostname()
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, "(^_^)  Hostname:  %s  (x_x)  %s", name, vs)
}
