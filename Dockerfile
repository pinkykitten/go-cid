FROM golang:1.10.1-alpine as builder
ARG REPO_NAME=gitlab.com/hientt53/go-cid
WORKDIR  /go/src/$REPO_NAME
ADD ./ /go/src/$REPO_NAME
RUN apk --update add git openssh && \
    rm -rf /var/lib/apt/lists/* && \
    rm /var/cache/apk/*
RUN go get -u github.com/golang/dep/...
RUN dep ensure
RUN go install $REPO_NAME


FROM alpine:3.7
RUN apk --update add ca-certificates 
RUN apk add --no-cache tzdata && \
    cp -f /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime && \
    apk del tzdata && \
    rm -rf /var/lib/apt/lists/* && \
    rm /var/cache/apk/*
WORKDIR /app
COPY ./config/prod.yaml /
COPY ./config/staging.yaml /
COPY ./config/dev.yaml /
COPY --from=builder /go/bin/go-cid /app/
ENTRYPOINT ./go-cid
EXPOSE 3214

